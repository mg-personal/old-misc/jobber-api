# Jobber-API

## About ️‼️ ❌

This is the API for my Jobber app. It's a work in progress and by so you should not
expect 100% functionality or a bug-free experience.

## Technology 💡

- [MongoDB](https://www.mongodb.com)
- [Express.js](https://expressjs.com)
- [Node.js](https://nodejs.org/en/)
- [PostgreSQL](https://www.postgresql.org)

## License ⚠️

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
