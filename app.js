const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const port = 3030;

const { Pool, Client } = require("pg");

// Database first connection configuration

const pool = new Pool({
  user: "maurogarcia",
  host: "localhost",
  database: "testdb",
  password: "mauro922290",
  port: 5433,
});

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// GET for users list

app.get("/", (req, res) => {
  pool.query("SELECT * FROM myuser;", (err, response) => {
    if (err) {
      console.log(err);
    } else {
      res.send(response.rows);
    }
    // pool.end();
  });
});

// POST for creating user.

app.post("/create-user", (req, res) => {
  let userName = "camicelia";
  let userPassword = "camicelia";

  const text = `CREATE ROLE camicelia WITH 
    LOGIN 
    SUPERUSER 
    CREATEDB 
    CREATEROLE 
    INHERIT 
    REPLICATION 
    CONNECTION LIMIT -1 
    PASSWORD 'camicelia'`;

  pool.query(text, (error, response) => {
    if (error) {
      console.log(error);
    } else {
      res.send("user created");
    }
  });
});

// POST for changing ROLE to current created user

app.post("/change-user", (req, res) => {
  const text = `SET ROLE camicelia`;
  const checkUserText = "SELECT current_user";

  pool.query(text, (error, response) => {
    if (error) {
      console.log(error);
    } else {
      pool.query(checkUserText, (error2, response2) => {
        if (error2) {
          console.log(error2);
        } else {
          res.send(response2.rows[0].current_user);
        }
      });
    }
  });
});

// Post for adding data to myuser table

app.post("/add-user", (req, res) => {
  let userName = req.body.name;
  let userAddress = req.body.address;

  const text = "INSERT INTO myuser (name, address) VALUES ($1, $2) RETURNING *";
  const values = [userName, userAddress];

  pool.query(text, values, (error, response) => {
    if (error) {
      console.log(error);
    } else {
      console.log(response.rows);
    }
  });
});

// POST for deleting data from myuser

app.post("/delete-user", (req, res) => {
  let userName = req.body.name;
  let userAddress = req.body.address;

  const text = "DELETE FROM myuser WHERE name = $1 RETURNING *";
  const values = [userName];

  pool.query(text, values, (error, response) => {
    if (error) {
      console.log(error);
    } else {
      console.log("User deleted.");
    }
  });
});

app.listen(port, () => console.log("Listening on port:" + port));
